[[_TOC_]]

# Vision produit
Module ayant pour but d'intégrer dans une base de données des données extraites d'un fichier Excel

#### POUR :
Petites boutiques

#### QUI VEULENT :
Ajouter des commandes dans la base de données et mettre à jour la base des produits (les clients sont intégrés dans la table commandes)

#### PRODUIT :
Mettre à jour une base de données client

#### EST UN :
Service de données client

#### QUI :
Automatique


# Gestion des risques

#### Technique:

- Problème pour lire un fichier Excel - Prévoir un autre type de fichier    - Très probable   - Gravité moyenne
- Problème version de JDK Java - Downgrade la version de Java               - Probable        - Gravité moyenne
- Le serveur tombe en panne - Prévoir des backup                            - Moins probable  - Gravité élevée
- Injection SQL - Requetes préparées                                        - Moins probable  - Gravité élevée

#### Externes (clients, utilisateurs…) :

- L'utilisateur intègre des données erronées - vérification des données avant import
- L'utilisateur selectionne un fichier non conforme - vérification du format du fichier
- Le fichier est trop volumineux - vérification de la taille du fichier

#### Organisationnel :

- Le référent technique du client est incompétent ou absent - Demander un autre référent technique

#### Gestion de projet :

- Retard sur un sprint ou livraison du produit final - Demander un prolongement du spring
- Un des membre du projet est indisponible - Se répartir les tâches de l'absent 
- Le PSG a perdu en demi alors que c'etait leur meilleur saison depuis 10 ans, Alain est en depression
- Lucas n'a pas internet et joue a la ps4
- Olivier se prend pour saken et joue 10 games de lol
- Alexandre a des problèmes avec sa voiture

