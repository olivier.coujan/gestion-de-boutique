import UI.FileSelector;
import java.util.Scanner;

import FileReader.FileReaderOrder;
import FileReader.FileReaderProduct;
import Models.Order.Order;
import Models.Product.Product;

import java.util.ArrayList;


public class Main {

    public static void main(String[] args) {

        FileSelector fileSelector = new FileSelector();

        ArrayList<Product> liProducts = new ArrayList<>();
        ArrayList<Order> liOrder = new ArrayList<>();

        Scanner in = new Scanner(System.in);
        int choix = -1;



        while (choix != 0){

            System.out.println("1 - Importer un fichier d'articles");
            System.out.println("2 - Importer un fichier de commandes");
            System.out.println("0 - Quitter");
            choix = in.nextInt();

            switch (choix){

                case 1:
                    fileSelector.openRead();

                    FileReaderProduct fileProduct = new FileReaderProduct(fileSelector.getChemin());

                    liProducts = fileProduct.ReadFileProducts();

                    for (int i = 0; i< liProducts.size(); i++){

                        System.out.println(liProducts.get(i).getProductRef() + " - " + liProducts.get(i).getDesignation() + " -> " + liProducts.get(i).getPrice() + " : " + liProducts.get(i).getStock());

                    }
                break;

                case 2:
                    fileSelector.openRead();

                    FileReaderOrder fileOrder = new FileReaderOrder(fileSelector.getChemin());

                    liOrder = fileOrder.ReadFileOrder();

                    for (int i = 0; i< liOrder.size(); i++){

                        System.out.println(liOrder.get(i).getNumCommade() + " -> " + liOrder.get(i).getAdress() + " - " + liOrder.get(i).getMail() + " : " + liOrder.get(i).getPhone());

                        for (int e = 0; e<liOrder.get(i).getOrderLines().size(); e++){

                            System.out.println(liOrder.get(i).getOrderLines().get(e).getProductRef() + " -> " + liOrder.get(i).getOrderLines().get(e).getQte());

                        }

                    }

                    break;

                case 0:

                    System.out.println("Bye bye !");

                    break;

                default:

                    System.out.println("Instruction incorrect ! Merci de recommencer.");

                    break;

            }

        }

    }

}
