package Models.Product;

import DataBase.MySqlConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Optional;

public class ProductMySql {

    private MySqlConnection sqlCon;
    Connection con;

    public ProductMySql() {
        this.sqlCon = new MySqlConnection();
        con = sqlCon.getCon();
    }

    public Optional<Product> findProductByRef(String productRef){

        String productQuery =
            """
                select A_Reference, A_Designation, A_Stock, A_Prix
                from articles
                where A_Reference = ?""";

        try{
            //Prepare Product query
            PreparedStatement stmtProduct = this.con.prepareStatement(productQuery);
            stmtProduct.setString(1, productRef);

            //Execute Product query
            var optResProduct = sqlCon.ExecutePreparedReqQuery(stmtProduct);
            if(optResProduct.isPresent()){

                //Read Product query
                var resOrder = optResProduct.get();
                resOrder.next();

                //Get Product columns
                String A_Reference      = resOrder.getString("A_Reference");
                String A_Designation    = resOrder.getString("A_Designation");
                int A_Stock             = resOrder.getInt("A_Stock");
                double A_Prix           = resOrder.getDouble("A_Prix");

                return Optional.of(new Product(A_Reference, A_Designation, A_Stock, A_Prix));
            }

        }catch (Exception e){
            System.out.println(e);
        }

        return Optional.empty();
    }

    public int insertProduct(Product product){

        String insertProduct =
            """
                insert into articles
                (A_Reference, A_Designation, A_Stock, A_Prix)
                values (?, ?, ?, ?)""";

        String A_Reference = product.getProductRef();
        String A_Designation = product.getDesignation();
        int A_Stock = product.getStock();
        double A_Prix = product.getPrice();

        try{
            //Prepare Order insert
            PreparedStatement stmtOrderInsert = this.con.prepareStatement(insertProduct);
            stmtOrderInsert.setString(1, A_Reference);
            stmtOrderInsert.setString(2, A_Designation);
            stmtOrderInsert.setInt(3, A_Stock);
            stmtOrderInsert.setDouble(4, A_Prix);

            System.out.println(stmtOrderInsert.toString());

            //Result insert
            return sqlCon.ExecutePreparedReqDataManip(stmtOrderInsert);

        }catch (Exception e){
            System.out.println(e);
        }

        return 0;
    }


}
