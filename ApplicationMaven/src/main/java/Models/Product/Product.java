package Models.Product;

public class Product {

    private String productRef;
    private String designation;
    private int stock;
    private double price;

    //Constructor
    public Product(String productRef, String designation, int stock, double price) {
        this.productRef = productRef;
        this.designation = designation;
        this.stock = stock;
        this.price = price;
    }

    public String getProductRef() {
        return productRef;
    }

    public String getDesignation() {
        return designation;
    }

    public int getStock() {
        return stock;
    }

    public double getPrice() {
        return price;
    }
}
