package Models.Product;

import java.util.Optional;

public class ProductController {

    ProductMySql productRepository = new ProductMySql();

    public ProductController() {
    }

    public Optional<Product> getProductByRef(String reference){
        var optProduct = productRepository.findProductByRef(reference);
        if(optProduct.isPresent()){
            return Optional.of(optProduct.get());
        }
        return Optional.empty();
    }

    public boolean existProduct(String reference){
        return productRepository.findProductByRef(reference).isPresent();
    }

    public boolean insertProduct(Product product){
        if(existProduct(product.getProductRef())) return false;
        return productRepository.insertProduct(product) > 0;

    }

}
