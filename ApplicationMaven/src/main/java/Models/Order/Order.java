package Models.Order;

import java.util.ArrayList;

public class Order {

    private int orderID;
    private String numOrder;
    private String address;
    private String mail;
    private String phone;
    private ArrayList<OrderLine> orderLines;

    public Order(int orderID, String numOrder, String address, String mail, String phone, ArrayList<OrderLine> orderLines) {
        this.orderID = orderID;
        this.numOrder = numOrder;
        this.address = address;
        this.mail = mail;
        this.phone = phone;
        this.orderLines = orderLines;
    }

    public Order(String numOrder, String address, String mail, String phone, ArrayList<OrderLine> orderLines) {
        this.numOrder = numOrder;
        this.address = address;
        this.mail = mail;
        this.phone = phone;
        this.orderLines = orderLines;
    }

    public int getOrderID() {
        return orderID;
    }

    public String getNumOrder() {
        return numOrder;
    }

    public String getAddress() {
        return address;
    }

    public String getMail() {
        return mail;
    }

    public String getAmount() {
        return amount;
    }

    public String getPhone() {
        return phone;
    }

    public ArrayList<OrderLine> getOrderLines() {
        return orderLines;
    }
}
