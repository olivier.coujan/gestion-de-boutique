package Models.Order;

public class OrderLine {

    private int orderLineID;
    private int orderID;
    private String productRef;
    private int qte;

    public OrderLine(int orderLineID, int orderID, String productRef, int qte) {
        this.orderLineID = orderLineID;
        this.orderID = orderID;
        this.productRef = productRef;
        this.qte = qte;
    }

    public OrderLine(String productRef, int qte) {
        this.productRef = productRef;
        this.qte = qte;
    }

    public int getOrderLineID() {
        return orderLineID;
    }

    public int getOrderID() {
        return orderID;
    }

    public String getProductRef() {
        return productRef;
    }

    public int getQte() {
        return qte;
    }
}
