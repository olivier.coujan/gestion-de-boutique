package Models.Order;

import DataBase.MySqlConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Optional;

public class OrderMySQL {

    private MySqlConnection sqlCon;
    Connection con;

    public OrderMySQL() {
        this.sqlCon = new MySqlConnection();
        con = sqlCon.getCon();
    }

    public Optional<Order> findOrderByNumOrder(String numOrder){
        String orderQuery = "select * from commandes where C_NUM = ?";
        String orderLineQuery = "select * from commande_ligne where C_ID = ?";

        try{
            //Prepare Order query
            PreparedStatement stmtOrder = this.con.prepareStatement(orderQuery);
            stmtOrder.setString(1, numOrder);



            //Execute Order query
            var optResOrder = sqlCon.ExecutePreparedReqQuery(stmtOrder);
            if(optResOrder.isPresent()){

                //Read Order query
                var resOrder = optResOrder.get();
                resOrder.next();
                //Get Order columns
                int C_ID = resOrder.getInt("C_ID");
                String C_NUM = resOrder.getString("C_NUM");
                String C_Adresse = resOrder.getString("C_Adresse");
                String C_Mail = resOrder.getString("C_Mail");
                String C_Telephone = resOrder.getString("C_Telephone");

                //Prepare query Order lines
                PreparedStatement stmtOrderLine = this.con.prepareStatement(orderLineQuery);
                stmtOrderLine.setInt(1, C_ID);

                //Execute Order lines query
                var optResOrderLine = sqlCon.ExecutePreparedReqQuery(stmtOrderLine);

                //Read Order lines query
                ArrayList<OrderLine> orderLines = new ArrayList<>();
                if(optResOrderLine.isPresent()){
                    try{
                        ResultSet resOrderLine = optResOrderLine.get();
                        while (resOrderLine.next())
                        {

                            //Get order lines columns
                            int CL_ID = resOrderLine.getInt(1);
                            String A_Reference = resOrderLine.getString(3);
                            int CL_Qte = resOrderLine.getInt(4);

                            orderLines.add(new OrderLine(CL_ID, C_ID, A_Reference, CL_Qte));
                        }

                    }catch (Exception e){
                        System.out.println(e);
                        return Optional.empty();
                    }
                }
                return Optional.of(new Order(C_ID, C_NUM, C_Adresse, C_Mail, C_Telephone, orderLines));
            }

        }catch (Exception e){
            System.out.println(e);
            return Optional.empty();
        }

        return Optional.empty();
    }

    public boolean insertOrder(Order order){
        String insertOrder = "insert into commandes (C_NUM, C_Adresse, C_Mail, C_Telephone) values (?, ?, ?, ?)";
        String insertOrderLine = "insert into commande_ligne (C_ID, A_Reference, CL_Qte) values ((select max(C_ID) from commandes), ?, ?)";

        String C_Num = order.getNumOrder();
        String C_Adresse = order.getAddress();
        String C_Mail = order.getMail();
        String C_Telephone = order.getPhone();
        ArrayList<OrderLine> orderLines = order.getOrderLines();

        try{
            //Prepare Order insert
            PreparedStatement stmtOrderInsert = this.con.prepareStatement(insertOrder);
            stmtOrderInsert.setString(1, C_Num);
            stmtOrderInsert.setString(2, C_Adresse);
            stmtOrderInsert.setString(3, C_Mail);
            stmtOrderInsert.setString(4, C_Telephone);

            //Result insert
            int resInsertOrder = sqlCon.ExecutePreparedReqDataManip(stmtOrderInsert);
            if(resInsertOrder < 1)return false;

            for (OrderLine orderLine : orderLines) {
                String productRef = orderLine.getProductRef();
                int qte = orderLine.getQte();
                PreparedStatement stmtOrderLineInsert = this.con.prepareStatement(insertOrderLine);
                stmtOrderLineInsert.setString(1, productRef);
                stmtOrderLineInsert.setInt(2, qte);
                int resInsertLine = sqlCon.ExecutePreparedReqDataManip(stmtOrderLineInsert);

                if(resInsertLine < 1)return false;
            }

        }catch (Exception e){
            System.out.println(e);
            return false;
        }

        return true;
    }

    public boolean deleteOrder(String numOrder){


        return true;
    }
}
