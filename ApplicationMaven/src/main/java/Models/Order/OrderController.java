package Models.Order;

import java.util.Optional;

public class OrderController {

    OrderMySQL repository = new OrderMySQL();


    public OrderController() {}

    public Boolean existOrder(String numOrder){return repository.findOrderByNumOrder(numOrder).isPresent();}

    public Optional<Order> getOrderById(String numOrder){return repository.findOrderByNumOrder(numOrder);}

    public boolean createOrder(Order order){
        if (existOrder(order.getNumOrder())) return false;
        return repository.insertOrder(order);
    }

    public boolean deleteOrder(String numOrder){return repository.deleteOrder(numOrder);}

}
