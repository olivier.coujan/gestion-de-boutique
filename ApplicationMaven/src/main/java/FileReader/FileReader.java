package FileReader;

import java.util.Locale;

public class FileReader {

    protected String filePath;
    protected int isXlSX = -1;

    public FileReader(String filePath){

        this.filePath = filePath;

        String[] parse = filePath.split("\\.");

        String extention = parse[parse.length-1].toLowerCase();

        if (extention.equals("xlsx")){

            this.isXlSX = 1;

        }else if (extention.equals("xls")){

            this.isXlSX = 0;

        }

    }

    public int getIsXlSX() {
        return isXlSX;
    }

}
