package FileReader;

import Models.Order.Order;
import Models.Order.OrderLine;
import Models.Product.Product;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class FileReaderProduct extends FileReader{

    public FileReaderProduct (String filePath){

        super(filePath);

    }

    public ArrayList<Product> ReadFileProducts(){

        ArrayList<Product> liProduct = new ArrayList<>();

        if (this.getIsXlSX() == 1){

            liProduct = ReadFileProductsXLSX();

        }else if (this.getIsXlSX() == 0) {

            liProduct = ReadFileProductsXLS();

        }

        return liProduct;

    }

    public ArrayList<Product> ReadFileProductsXLSX(){

        ArrayList<Product> liProduct = new ArrayList<>();

        FileInputStream inputStream;
        XSSFWorkbook workbook;
        File file;
        XSSFSheet sheet;

        XSSFCell RefProduct;
        XSSFCell Designation;
        XSSFCell Qte;
        XSSFCell Price;

        String sRefProduct;
        String sDesignation;

        int nQte;

        double rPrice;

        //Récupération du fichier XLS/XLSX
        file = new File(this.filePath);

        try {

            //Ouverture du fichier XLS/XLSX
            inputStream = new FileInputStream(file);

            //Récupération le classeur du fichier XLS/XLSX
            workbook = new XSSFWorkbook(inputStream);

            //Récupération de la feuille du classeur
            sheet = workbook.getSheetAt(0);

            for (int i = 1;  i <= sheet.getLastRowNum(); i++){

                RefProduct = sheet.getRow(i).getCell(0);
                sRefProduct = RefProduct.getStringCellValue();

                Designation = sheet.getRow(i).getCell(1);
                sDesignation = Designation.getStringCellValue();

                Qte = sheet.getRow(i).getCell(2);
                nQte = (int)Qte.getNumericCellValue();

                Price = sheet.getRow(i).getCell(3);
                rPrice = Price.getNumericCellValue();

                Product Article = new Product(sRefProduct, sDesignation, nQte, rPrice);

                liProduct.add(Article);

            }

        }catch (FileNotFoundException e){

            System.out.println("FileNotFoundException -> " + e);

        }catch (IOException e){

            System.out.println("IOException -> " + e);

        }catch (NullPointerException e){

            System.out.println("NullPointerException -> " + e);

        }

        return liProduct;

    }

    public ArrayList<Product> ReadFileProductsXLS(){

        ArrayList<Product> liProduct = new ArrayList<>();

        FileInputStream inputStream;
        HSSFWorkbook workbook;
        File file;
        HSSFSheet sheet;

        HSSFCell RefProduct;
        HSSFCell Designation;
        HSSFCell Qte;
        HSSFCell Price;

        String sRefProduct;
        String sDesignation;

        int nQte;

        double rPrice;

        //Récupération du fichier XLS/XLSX
        file = new File(this.filePath);

        try {

            //Ouverture du fichier XLS/XLSX
            inputStream = new FileInputStream(file);

            //Récupération le classeur du fichier XLS/XLSX
            workbook = new HSSFWorkbook(inputStream);

            //Récupération de la feuille du classeur
            sheet = workbook.getSheetAt(0);

            for (int i = 1;  i <= sheet.getLastRowNum(); i++){

                RefProduct = sheet.getRow(i).getCell(0);
                sRefProduct = RefProduct.getStringCellValue();

                Designation = sheet.getRow(i).getCell(1);
                sDesignation = Designation.getStringCellValue();

                Qte = sheet.getRow(i).getCell(2);
                nQte = (int)Qte.getNumericCellValue();

                Price = sheet.getRow(i).getCell(3);
                rPrice = Price.getNumericCellValue();

                Product Article = new Product(sRefProduct, sDesignation, nQte, rPrice);

                liProduct.add(Article);

            }

        }catch (FileNotFoundException e){

            System.out.println("FileNotFoundException -> " + e);

        }catch (IOException e){

            System.out.println("IOException -> " + e);

        }catch (NullPointerException e){

            System.out.println("NullPointerException -> " + e);

        }

        return liProduct;

    }

}
