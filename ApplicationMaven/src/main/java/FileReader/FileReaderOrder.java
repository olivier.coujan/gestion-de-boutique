package FileReader;

import Models.Order.Order;
import Models.Order.OrderLine;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class FileReaderOrder extends FileReader{

    public FileReaderOrder (String filePath){

        super(filePath);

    }

    public ArrayList<Order> ReadFileOrder(){

        ArrayList<Order> liOrder = new ArrayList<>();

        if (this.getIsXlSX() == 1){

            liOrder = ReadFileOrderXLSX();

        }else if (this.getIsXlSX() == 0) {

            liOrder = ReadFileOrderXLS();

        }

        return liOrder;

    }

    public ArrayList<Order> ReadFileOrderXLSX(){

        ArrayList<Order> liOrder = new ArrayList<>();
        ArrayList<OrderLine> liOrderLine = new ArrayList<>();

        FileInputStream inputStream;
        XSSFWorkbook workbook;
        File file;
        XSSFSheet sheet;

        XSSFCell NumCom;
        XSSFCell NextNumCom;
        XSSFCell Adress;
        XSSFCell Mail;
        XSSFCell Phone;
        XSSFCell Article;
        XSSFCell Qte;

        String sAdress;
        String sMail;
        String sPhone;
        String sNextNumCom;

        int nNbRow;

        //Récupération du fichier XLS/XLSX
        file = new File(this.filePath);

        try {

            //Ouverture du fichier XLS/XLSX
            inputStream = new FileInputStream(file);

            //Récupération le classeur du fichier XLS/XLSX
            workbook = new XSSFWorkbook(inputStream);

            //Récupération de la feuille du classeur
            sheet = workbook.getSheetAt(0);

            //Nombre de lignes du fichier
            nNbRow = sheet.getLastRowNum();

            for (int i = 1;  i <= nNbRow; i++){

                Article = sheet.getRow(i).getCell(4);
                String sArticle = Article.getStringCellValue();

                Qte = sheet.getRow(i).getCell(5);
                int iQte = (int) Qte.getNumericCellValue();

                OrderLine orderLine = new OrderLine(sArticle, iQte);

                liOrderLine.add(orderLine);

                NumCom = sheet.getRow(i).getCell(0);
                String sNumCom = NumCom.getStringCellValue();

                if (i != nNbRow) {

                    NextNumCom = sheet.getRow(i + 1).getCell(0);
                    sNextNumCom = NextNumCom.getStringCellValue();

                }else{

                    sNextNumCom = "";

                }

                if (!sNumCom.equals(sNextNumCom)){

                    Adress = sheet.getRow(i).getCell(1);
                    sAdress = Adress.getStringCellValue();

                    Mail = sheet.getRow(i).getCell(2);
                    sMail = Mail.getStringCellValue();

                    Phone = sheet.getRow(i).getCell(3);
                    sPhone = Phone.getStringCellValue();

                    Order order = new Order(0, sNumCom, sAdress, sMail, sPhone, liOrderLine);

                    liOrder.add(order);

                    liOrderLine = new ArrayList<>();

                }

            }

        }catch (FileNotFoundException e){

            System.out.println("FileNotFoundException -> " + e);

        }catch (IOException e){

            System.out.println("IOException -> " + e);

        }catch (NullPointerException e){

            System.out.println("NullPointerException -> " + e);

        }

        return liOrder;

    }

    public ArrayList<Order> ReadFileOrderXLS(){

        ArrayList<Order> liOrder = new ArrayList<>();
        ArrayList<OrderLine> liOrderLine = new ArrayList<>();

        FileInputStream inputStream;
        HSSFWorkbook workbook;
        File file;
        HSSFSheet sheet;

        HSSFCell NumCom;
        HSSFCell NextNumCom;
        HSSFCell Adress;
        HSSFCell Mail;
        HSSFCell Phone;
        HSSFCell Article;
        HSSFCell Qte;

        String sAdress;
        String sMail;
        String sPhone;
        String sNextNumCom;

        int nNbRow;

        //Récupération du fichier XLS/XLSX
        file = new File(this.filePath);

        try {

            //Ouverture du fichier XLS/XLSX
            inputStream = new FileInputStream(file);

            //Récupération le classeur du fichier XLS/XLSX
            workbook = new HSSFWorkbook(inputStream);

            //Récupération de la feuille du classeur
            sheet = workbook.getSheetAt(0);

            //Nombre de lignes du fichier
            nNbRow = sheet.getLastRowNum();

            for (int i = 1;  i <= nNbRow; i++){

                Article = sheet.getRow(i).getCell(4);
                String sArticle = Article.getStringCellValue();

                Qte = sheet.getRow(i).getCell(5);
                int iQte = (int) Qte.getNumericCellValue();

                OrderLine orderLine = new OrderLine(sArticle, iQte);

                liOrderLine.add(orderLine);

                NumCom = sheet.getRow(i).getCell(0);
                String sNumCom = NumCom.getStringCellValue();

                if (i != nNbRow) {

                    NextNumCom = sheet.getRow(i + 1).getCell(0);
                    sNextNumCom = NextNumCom.getStringCellValue();

                }else{

                    sNextNumCom = "";

                }

                if (!sNumCom.equals(sNextNumCom)){

                    Adress = sheet.getRow(i).getCell(1);
                    sAdress = Adress.getStringCellValue();

                    Mail = sheet.getRow(i).getCell(2);
                    sMail = Mail.getStringCellValue();

                    Phone = sheet.getRow(i).getCell(3);
                    sPhone = Phone.getStringCellValue();

                    Order order = new Order(0, sNumCom, sAdress, sMail, sPhone, liOrderLine);

                    liOrder.add(order);

                    liOrderLine = new ArrayList<>();

                }

            }

        }catch (FileNotFoundException e){

            System.out.println("FileNotFoundException -> " + e);

        }catch (IOException e){

            System.out.println("IOException -> " + e);

        }catch (NullPointerException e){

            System.out.println("NullPointerException -> " + e);

        }


        return liOrder;

    }

}
